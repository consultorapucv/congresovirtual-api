*Esta herramienta digital forma parte del catálogo de herramientas del **Banco Interamericano de Desarrollo**. Puedes conocer más sobre la iniciativa del BID en [code.iadb.org](https://code.iadb.org)*


## Congreso Virtual

### Description
---
This is the source code for the website of Congreso virtual, a website for citizen participation linked to legislative process.  

This repository contains the files needed to set up the server and the website of Congreso virtual.Note that to set up a server.You also need a server computer.

You can use your own computer as a server, but for other people to connect to your computer, you'll need to expose a port (default is 80 but you can choose a different one) to connect to, which sometimes requires port forwarding. Note that some internet providers don't let you host a server at all, in which case you'll have to rent a VPS to use as a server.

### User guide
---
The website has diferent sections, for the citizen participation and learning:

- Discusiones: Has the discussions of the citizen. The user can create a discusion for a bill or an article of bill. The users can rate the discussion if like or not, also can comment and rate te comments.
- Proyectos: Has the bills in progress for votation or finished. Here the citizens can see the information of the project. The user can create a new proposition for the project,also can vote or coment the propositions. Finally, the projects have articles where the user can rate or coment, this part was attached to the results of the project.
- Procesos: Has the bills in progress actually in the congress and the information about it.
- Instructivo: Has the instructions for the right use for the website sections. 
 	
### Instalation guide
---
Paso a paso de cómo instalar la herramienta digital. En esta sección es recomendable explicar la arquitectura de carpetas y módulos que componen el sistema.

Según el tipo de herramienta digital, el nivel de complejidad puede variar. En algunas ocasiones puede ser necesario instalar componentes que tienen dependencia con la herramienta digital. Si este es el caso, añade también la siguiente sección.

La guía de instalación debe contener de manera específica:
- Los requisitos del sistema operativo para la compilación (versiones específicas de librerías, software de gestión de paquetes y dependencias, SDKs y compiladores, etc.).
- Las dependencias propias del proyecto, tanto externas como internas (orden de compilación de sub-módulos, configuración de ubicación de librerías dinámicas, etc.).
- Pasos específicos para la compilación del código fuente y ejecución de tests unitarios en caso de que el proyecto disponga de ellos.

##### Requeriments
- PHP 7.1.3
- git 2+

##### Step by step
1) Clonar repostorio.

        git clone {repository url}

2) Ingresar a la carpeta del repositorio.

#### Dependencias
Descripción de los recursos externos que generan una dependencia en la herramienta digital (librerías, frameworks, acceso a bases de datos y licencias de cada recurso). 

- Laravel 5.8
- OpenAPI 3
- PHPUnit 7.5

### Contribution
---
Esta sección explica a desarrolladores cuáles son las maneras habituales de enviar una solicitud de adhesión de nuevo código (“pull requests”), cómo declarar fallos en la herramienta y qué guías de estilo se deben usar al escribir más líneas de código. También puedes hacer un listado de puntos que se pueden mejorar de tu código para crear ideas de mejora.

### Código de conducta 
---
El código de conducta establece las normas sociales, reglas y responsabilidades que los individuos y organizaciones deben seguir al interactuar de alguna manera con la herramienta digital o su comunidad. Es una buena práctica para crear un ambiente de respeto e inclusión en las contribuciones al proyecto. 

La plataforma Github premia y ayuda a los repositorios dispongan de este archivo. Al crear CODE_OF_CONDUCT.md puedes empezar desde una plantilla sugerida por ellos. Puedes leer más sobre cómo crear un archivo de Código de Conducta (aquí)[https://help.github.com/articles/adding-a-code-of-conduct-to-your-project/]

### Colaboradores
---

- Ph.D. Claudio Cubillos - [PUCV](http://www.inf.ucv.cl/academicos/claudio-cubillos-figueroa/)
- Rafael Mellado - [WEB](http://rafaelmellado.cl/)
- Johann Valenzuela - [Github](https://github.com/johannvalenzuela) - [Bitbucket](https://bitbucket.org/johannvalenzuela)
- Enzo Barbaguelatta - [WEB](http://elsemieni.net/)


### Información adicional
---
Esta es la sección que permite agregar más información de contexto al proyecto como alguna web de relevancia, proyectos similares o que hayan usado la misma tecnología.

### MIT License 
---

[LICENCIA](https://github.com/EL-BID/Plantilla-de-repositorio/blob/master/LICENSE.md)

La licencia especifica los permisos y las condiciones de uso que el desarrollador otorga a otros desarrolladores que usen y/o modifiquen la herramienta digital.

Incluye en esta sección una note con el tipo de licencia otorgado a esta herramienta digital. El texto de la licencia debe estar incluído en un archivo *LICENSE.md* o *LICENSE.txt* en la carpeta raíz.

Si desconoces qué tipos de licencias existen y cuál es la mejor para cada caso, te recomendamos visitar la página https://choosealicense.com/.

## Limitación de responsabilidades

El BID no será responsable, bajo circunstancia alguna, de daño ni indemnización, moral o patrimonial; directo o indirecto; accesorio o especial; o por vía de consecuencia, previsto o imprevisto, que pudiese surgir:

i. Bajo cualquier teoría de responsabilidad, ya sea por contrato, infracción de derechos de propiedad intelectual, negligencia o bajo cualquier otra teoría; y/o

ii. A raíz del uso de la Herramienta Digital, incluyendo, pero sin limitación de potenciales defectos en la Herramienta Digital, o la pérdida o inexactitud de los datos de cualquier tipo. Lo anterior incluye los gastos o daños asociados a fallas de comunicación y/o fallas de funcionamiento de computadoras, vinculados con la utilización de la Herramienta Digital.